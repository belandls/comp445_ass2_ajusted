#pragma once
#include "Command.h"
class RenamefileCommand :
	public Command
{
private:
	string _file_to_rename;
public:
	RenamefileCommand();
	virtual ~RenamefileCommand();

	// Inherited via Command
	virtual bool executeCommand(const char *) override;
	virtual bool performInitialNego() override;
	virtual bool performDataTransfer() override;
};


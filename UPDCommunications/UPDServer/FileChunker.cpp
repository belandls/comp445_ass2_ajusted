/* 
 * File:   FileChunker.cpp
 * Author: Samuel Beland-Leblanc - 27185642
 * Description : FileChunker class implementation
 * Created on February 10, 2016, 12:18 PM
 */

#include "FileChunker.h"

const int FileChunker::CHUNK_MAX_SIZE = 1024;

int FileChunker::getFileSize() {
    return _file_size;
}

int FileChunker::getLastChunkSize() {
    return _last_chunk_size;
}

FileChunker::FileChunker() {
    _last_chunk_read = NULL;
    _last_chunk_size = 0;
    _file_size = 0;
    _in_stream = NULL;
    _is_eof = false;
    _is_loaded = false;
}

FileChunker::FileChunker(const FileChunker& orig) {
}

FileChunker::~FileChunker() {
    delete[] _last_chunk_read;
    _last_chunk_read = NULL;
    _in_stream->close();
    delete _in_stream;
    _in_stream = NULL;
}

//Get the next chunk of size CHUNK_MAX_SIZE from the file 

const char* FileChunker::getNextChunk() {

    //Check if a file has been loaded
    if (_is_loaded) {
        //Check if the whole file was read
        if (!_is_eof) {
            //Free the last chunk to save memory
            if (_last_chunk_read != NULL) {
                delete[] _last_chunk_read;
                _last_chunk_read = NULL;
                _last_chunk_size = 0;
            }
            //Read the next chunk and get the size of that chunk
            _last_chunk_read = new char[CHUNK_MAX_SIZE];
            _in_stream->read(_last_chunk_read, CHUNK_MAX_SIZE);
            _last_chunk_size = _in_stream->gcount();

            //check iff the last read hit the EOF
            if (*_in_stream) {
                return _last_chunk_read;
            } else { //EOF was met
                //if (_last_chunk_size > 0) {
                    _is_eof = true;
                    return _last_chunk_read;
                //}
            }
        }
    }
    _last_chunk_size = 0;
    return NULL;
}

bool FileChunker::isEOF() {
    return _is_eof;
}

//Opens the file to be chunked
bool FileChunker::loadFile(string filename) {
    //If a second file id loaded from the same object, free the last data structures
    if (_in_stream != NULL) {
        if (_last_chunk_read != NULL) {
            delete[] _last_chunk_read;
            _last_chunk_read = NULL;
            _last_chunk_size = 0;
        }
        _in_stream->close();
        delete _in_stream;
        _in_stream = NULL;
    }
    
    //Try to open the file to chunk
    _in_stream = new ifstream(filename, ifstream::binary);
    if (_in_stream->is_open()) {
        
        //Get the file length
        _in_stream->seekg(0, _in_stream->end);
        _file_size = _in_stream->tellg();
        _in_stream->seekg(0, _in_stream->beg);
        
        _is_loaded = true;
        return true;
    }
    return false;
}

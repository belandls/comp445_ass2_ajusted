/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

 /*
  * File:   ListFileCommand.cpp
  * Author: Samuel
  *
  * Created on February 9, 2016, 11:20 AM
  */

#include "ListFileCommand.h"
#include "Request.h"
#include <iostream>
#include<cstdio>
#include <windows.h>
#include <tchar.h> 
#include <stdio.h>
#include <strsafe.h>
#pragma comment(lib, "User32.lib")

ListFileCommand::ListFileCommand() {
}

ListFileCommand::ListFileCommand(const ListFileCommand& orig) {
}

ListFileCommand::~ListFileCommand() {
}

bool ListFileCommand::executeCommand(const char* addInfo) {
	WIN32_FIND_DATA ffd;
	LARGE_INTEGER filesize;
	TCHAR szDir[MAX_PATH];
	size_t length_of_arg;
	HANDLE hFind = INVALID_HANDLE_VALUE;
	DWORD dwError = 0;
	StringCchCopy(szDir, MAX_PATH, ".\\*");
	hFind = FindFirstFile(szDir, &ffd);
	do
	{
		if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			//_tprintf(TEXT("  %s   <DIR>\n"), ffd.cFileName);
		}
		else
		{
			char buff[256];
			int length = sprintf_s(buff, "%s\n", ffd.cFileName);
			_directory_data.append(buff, length);
		}	
	} while (FindNextFile(hFind, &ffd) != 0);
	FindClose(hFind);
	_directory_data.append('\0');
	//DIR *dir;
	//struct dirent *ent;
	//if ((dir = opendir(".")) != NULL) {
	//    /* print all the files and directories within directory */
	//    struct stat statbuf;
	//    while ((ent = readdir(dir)) != NULL) {
	//        if (stat(ent->d_name, &statbuf)) {
	//            continue;
	//        }
	//        if (S_ISDIR(statbuf.st_mode)){
	//            continue;
	//        }
	//        char buff[256];
	//        int length = sprintf(buff, "%s\n", ent->d_name);
	//        _directory_data.append(buff, length);
	//    }
	//    _directory_data.append('\0');
	//    closedir(dir);
	//} else {
	//    /* could not open directory */
	//    perror("");
	//    return false;
	//}

	Request r(Request::COMMAND_SUCCESFULL, 0);
	return GetCommunicationEntity()->Send(r.getRawByteRepresentation(), Request::REQUEST_SIZE);
}

bool ListFileCommand::performDataTransfer() {

	//    SOCKET currentSocket = getWorkingSocket();
	//    int res;
	//
	//    res = send(currentSocket, _directory_data.getRawData(), _directory_data.getLength(), 0);
	//    if (res == SOCKET_ERROR) {
	//        return false;
	//    }
	bool res = GetCommunicationEntity()->Send(reinterpret_cast<unsigned char*>(_directory_data.getRawData()), _directory_data.getLength());

	cout << "Waiting for send buffer to empty..." << endl;
	GetCommunicationEntity()->WaitUntilSendBufferEmpties();

	return res;
}

bool ListFileCommand::performInitialNego() {

	//    SOCKET currentSocket = getWorkingSocket();
	//    int res;
	//    char reqBuffer[260];
	//    Request sr(Request::READY_TO_SEND, _directory_data.getLength());
	//    res = send(currentSocket, sr.getRawByteRepresentation(), 260, 0);
	//    if (res == SOCKET_ERROR) {
	//        return false;
	//    }
	//
	//    res = recv(currentSocket, reqBuffer, 260, 0);
	//    if (res == SOCKET_ERROR) {
	//        return false;
	//    }
	//    Request r;
	//    r.fillFromRawByteRepresentation(reqBuffer);
	//    if (r.getRequestType() != Request::READY_TO_RECV) {
	//        return false;
	//    }

	Request sr(Request::READY_TO_SEND, _directory_data.getLength());
	GetCommunicationEntity()->Send(sr.getRawByteRepresentation(), Request::REQUEST_SIZE);
	const unsigned char* reqBuffer = GetCommunicationEntity()->Recv();
	if (reqBuffer == NULL) {
		return false;
	}
	Request r;
	r.fillFromRawByteRepresentation(reqBuffer);
	if (r.getRequestType() != Request::READY_TO_RECV) {
		return false;
	}

	return true;
}


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   PutFileCommand.cpp
 * Author: Sam
 * 
 * Created on February 10, 2016, 5:16 PM
 */

#include "PutFileCommand.h"
#include "DynamicArray.h"
#include "Request.h"
#include "../UPDCommunications/FileIO.h"

PutFileCommand::PutFileCommand() {
}

PutFileCommand::PutFileCommand(const PutFileCommand& orig) {
}

PutFileCommand::~PutFileCommand() {
}

bool PutFileCommand::executeCommand(const char* addInfo) {

	string fnTemp = addInfo;
	while (FileIO::FileExists(fnTemp)) {
		Request r(Request::FILE_ALREADY_EXISTS, 0);
		if (!GetCommunicationEntity()->Send(r.getRawByteRepresentation(), Request::REQUEST_SIZE)) {
			return false;
		}
		Request rr;
		const unsigned char* buffer = GetCommunicationEntity()->Recv();
		if (buffer == NULL) {
			return false;
		}
		rr.fillFromRawByteRepresentation(buffer);
		if (rr.getRequestType() != Request::FILE_ALREADY_EXISTS) {
			return false;
		}
		fnTemp = rr.getAddInfo();
	}
	if (fnTemp.empty()) {
		Request r(Request::FILE_ALREADY_EXISTS, 0);
		GetCommunicationEntity()->Send(r.getRawByteRepresentation(), Request::REQUEST_SIZE);
	}
	else {
		_out_stream = new ofstream(fnTemp, ofstream::binary);
		if (_out_stream->is_open()) {

			Request r(Request::COMMAND_SUCCESFULL, 0);
			if (!GetCommunicationEntity()->Send(r.getRawByteRepresentation(), Request::REQUEST_SIZE)) {
				return false;
			}

			return true;
		}
		else {
			Request r(Request::FILE_NOT_CREATED, 0);
			GetCommunicationEntity()->Send(r.getRawByteRepresentation(), Request::REQUEST_SIZE);
		}
	}
    
    return false;
}

bool PutFileCommand::performDataTransfer() {

    int receivedData = 0;
    while (receivedData < _size_of_data) {
        const unsigned char* buffer = GetCommunicationEntity()->Recv();
        if (buffer == NULL) {
            return false;
        } else {
            _out_stream->write(reinterpret_cast<const char*>(buffer), GetCommunicationEntity()->GetLastPacketDataSize());
            receivedData += GetCommunicationEntity()->GetLastPacketDataSize();
        }
    }
    _out_stream->close();
    return true;

}

bool PutFileCommand::performInitialNego() {

    Request rr;
    const unsigned char* buffer = GetCommunicationEntity()->Recv();
    if (buffer == NULL) {
        return false;
    }
    rr.fillFromRawByteRepresentation(buffer);
    if (rr.getRequestType() != Request::READY_TO_SEND) {
        return false;
    }
    _size_of_data = rr.getSizeOfData();
    Request sr(Request::READY_TO_RECV, 0);
    if(!GetCommunicationEntity()->Send(sr.getRawByteRepresentation(), Request::REQUEST_SIZE)){
        return false;
    }
    return true;
}

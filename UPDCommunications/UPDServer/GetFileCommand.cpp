/* 
 * File:   GetFileCommand.cpp
 * Author: Samuel Beland-Leblanc - 27185642
 * Description : GetFileCommand class implementation
 * Created on February 14, 2016, 3:37 PM
 */

#include "GetFileCommand.h"
#include "Request.h"
#include <iostream>
using namespace std;
GetFileCommand::GetFileCommand() {
}

GetFileCommand::GetFileCommand(const GetFileCommand& orig) {
}

GetFileCommand::~GetFileCommand() {
    delete _file_chunker;
}

//Tries to load the file to send to the client
bool GetFileCommand::executeCommand(const char* addInfo) {

    _file_chunker = new FileChunker();
    if (_file_chunker->loadFile(addInfo)){
        //If the file could be loaded, send a success command
        int res;
        Request sr(Request::COMMAND_SUCCESFULL,0);
        return GetCommunicationEntity()->Send(sr.getRawByteRepresentation(), Request::REQUEST_SIZE);
    }else{
        //If the file could not be loaded, send an error command
        Request sr(Request::FILE_NOT_FOUND,0);
        GetCommunicationEntity()->Send(sr.getRawByteRepresentation(), Request::REQUEST_SIZE);
    }
    return false;
}

//Send the file by chunks
bool GetFileCommand::performDataTransfer() {

    //While the EOF has not been met
    while(!_file_chunker->isEOF()){
        //Send the next chunk
        const unsigned char* data = reinterpret_cast<const unsigned char*>(_file_chunker->getNextChunk());
        int dataSize = _file_chunker->getLastChunkSize();
        if (!GetCommunicationEntity()->Send(data,dataSize)){
            return false;
        }
    }
	cout << "Waiting for send buffer to empty..." << endl;
	GetCommunicationEntity()->WaitUntilSendBufferEmpties();
    return true;
    
}

bool GetFileCommand::performInitialNego() {

    //First send a READY_TO_SEND with the filesize
    int res;
    Request sr(Request::READY_TO_SEND, _file_chunker->getFileSize());
    if(!GetCommunicationEntity()->Send(sr.getRawByteRepresentation(),Request::REQUEST_SIZE)){
        return false;
    }
    
    
    //After check if the client is ready to receive
    const unsigned char* buffer = GetCommunicationEntity()->Recv();
    if (buffer == NULL){
        return false;
    }
    Request rr;
    rr.fillFromRawByteRepresentation(buffer);
    if (rr.getRequestType() != Request::READY_TO_RECV){
        //If the client had an error, abort the command execution
        cout << "There was an unexpected error on the client's side" << endl;
        return false;
    }
    return true;
}


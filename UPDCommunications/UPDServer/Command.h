/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Command.h
 * Author: Samuel
 * Description : Command interface to be implemented by all the concrete implementations
 * Created on February 9, 2016, 11:18 AM
 */

#ifndef COMMAND_H
#define COMMAND_H

#include "CommunicationEntity.h"

class Command {
private:
    Communication::CommunicationEntity* _ce;
public:
	Command();
	virtual ~Command();

        void SetCommunicationEntity( Communication::CommunicationEntity*);
        Communication::CommunicationEntity* const GetCommunicationEntity() ;
	
        //The 3 steps of the protocol
	virtual bool executeCommand(const char *) = 0;
	virtual bool performInitialNego()=0;
	virtual bool performDataTransfer() = 0;

};

#endif /* COMMAND_H */


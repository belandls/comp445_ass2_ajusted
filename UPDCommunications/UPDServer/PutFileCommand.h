/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   PutFileCommand.h
 * Author: Sam
 *
 * Created on February 10, 2016, 5:16 PM
 */

#ifndef PUTFILECOMMAND_H
#define PUTFILECOMMAND_H

#include "Command.h"
#include <fstream>

using namespace std;
class PutFileCommand : public Command {
public:
    PutFileCommand();
    PutFileCommand(const PutFileCommand& orig);
    virtual ~PutFileCommand();
    
    bool executeCommand(const char*) override;
    bool performDataTransfer() override;
    bool performInitialNego() override;
    
private:
    ofstream* _out_stream;
    int _size_of_data;
};

#endif /* PUTFILECOMMAND_H */


#include "RenamefileCommand.h"
#include"Request.h"
#include"..\UPDCommunications\FileIO.h"

RenamefileCommand::RenamefileCommand()
{
}


RenamefileCommand::~RenamefileCommand()
{
}

bool RenamefileCommand::executeCommand(const char * addInfo)
{
	_file_to_rename = string(addInfo);
	if (FileIO::FileExists(_file_to_rename)) {
		//If the file could be loaded, send a success command
		int res;
		Request sr(Request::COMMAND_SUCCESFULL, 0);
		return GetCommunicationEntity()->Send(sr.getRawByteRepresentation(), Request::REQUEST_SIZE);
	}
	else {
		//If the file could not be loaded, send an error command
		Request sr(Request::FILE_NOT_FOUND, 0);
		GetCommunicationEntity()->Send(sr.getRawByteRepresentation(), Request::REQUEST_SIZE);
	}
	return false;
}

bool RenamefileCommand::performInitialNego()
{
	Request rr;
	const unsigned char* buffer = GetCommunicationEntity()->Recv();
	if (buffer == NULL) {
		return false;
	}
	rr.fillFromRawByteRepresentation(buffer);
	if (rr.getRequestType() != Request::READY_TO_SEND) {
		return false;
	}
	string fnTemp = string(rr.getAddInfo());
	while (FileIO::FileExists(fnTemp)) {
		Request r(Request::FILE_ALREADY_EXISTS, 0);
		if (!GetCommunicationEntity()->Send(r.getRawByteRepresentation(), Request::REQUEST_SIZE)) {
			return false;
		}
		Request rr;
		const unsigned char* buffer = GetCommunicationEntity()->Recv();
		if (buffer == NULL) {
			return false;
		}
		rr.fillFromRawByteRepresentation(buffer);
		if (rr.getRequestType() != Request::FILE_ALREADY_EXISTS) {
			return false;
		}
		fnTemp = rr.getAddInfo();
	}
	if (fnTemp.empty()) {
		Request r(Request::FILE_ALREADY_EXISTS, 0);
		GetCommunicationEntity()->Send(r.getRawByteRepresentation(), Request::REQUEST_SIZE);
	}
	else {
		if (FileIO::RenameFile(_file_to_rename, fnTemp)) {
			Request r(Request::READY_TO_RECV, 0);
			if (GetCommunicationEntity()->Send(r.getRawByteRepresentation(), Request::REQUEST_SIZE)) {
				return true;
			}
		}
		else {
			Request r(Request::FILE_ALREADY_EXISTS, 0);
			GetCommunicationEntity()->Send(r.getRawByteRepresentation(), Request::REQUEST_SIZE);
		}
	}
	return false;
}

bool RenamefileCommand::performDataTransfer()
{
	return true;
}

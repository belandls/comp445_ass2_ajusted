/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Server.cpp
 * Author: Samuel
 * 
 * Created on February 9, 2016, 11:20 AM
 */

#include "Server.h"
#include "ListFileCommand.h"
#include "PutFileCommand.h"
#include "Request.h"
#include "GetFileCommand.h"
#include "RenameFileCommand.h"
#include <iostream>
using namespace std;

const char * Server::DEFAULT_PORT = "55555";

Server::Server() {
    _s = new Communication::Server();
}

Server::~Server() {
}

Communication::Server* const Server::GetServer() const {
    return _s;
}


bool Server::init(int lossRate, int delayRate, int windowSize) {
	int realLossRate = (float)lossRate * 255 / 100;
	int realDelayRate = (float)delayRate * 255 / 100;
	_s->InitializeLog("server.txt");
    return _s->Initialize("9999",true,realLossRate, realDelayRate, windowSize);
}

void Server::mainListenLoop() {

    while (1) {
        cout << "Awaiting connection...";
        bool res = true;
        _s->AwaitConnection();
        if (!res) {
            
        } else {
            cout << "Connected." << endl;
            bool connected = true;
            while (connected) {
                
                const unsigned char* buffer  = _s->Recv();
                if (buffer == NULL) {
                    cout << "Disconnected" << endl;
                    connected = false;
                } else {
                    cout << "Command Received...";
                    Command* currentCommand = NULL;
                    Request rr;
                    rr.fillFromRawByteRepresentation(buffer);
                    if (rr.getRequestType() == Request::LIST_FILES) {
                        cout << "LIST_FILES" << endl;
                        currentCommand = new ListFileCommand();
                    } else if (rr.getRequestType() == Request::PUT_FILE) {
                        cout << "PUT_FILES" << endl;
                        currentCommand = new PutFileCommand();
                    }else if (rr.getRequestType() == Request::GET_FILE){
                        cout << "GET_FILES" << endl;
                        currentCommand = new GetFileCommand();
					}
					else if (rr.getRequestType() == Request::RENAME_FILE) {
						cout << "RENAME_FILES" << endl;
						currentCommand = new RenamefileCommand();
                    }
					if (currentCommand != NULL) {
						currentCommand->SetCommunicationEntity(_s);
						if (currentCommand->executeCommand(rr.getAddInfo().c_str())) {
							if (currentCommand->performInitialNego()) {
								if (currentCommand->performDataTransfer()) {
									cout << "Command + tranfer successful" << endl;
								}
								else {
									cout << "There was an error transferring the data" << endl;
								}
							}
							else {
								cout << "There was an error during the initial handshake" << endl;
							}
						}
						else {
							cout << "There was an error executing the command" << endl;
						}
						delete currentCommand;
					}
					else {
						cout << "Did not receive a command" << endl;
					}
                    
                }
            }
            cout << "Connection closed" << endl;
        }

    }


}


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Server.h
 * Author: Samuel
 *
 * Created on February 9, 2016, 11:20 AM
 */

#ifndef SERVER_HH
#define SERVER_HH

#include "../UPDCommunications/Server.h"


class Server {
public:
    Server();
    
    virtual ~Server();
    bool init(int,int, int);
    void mainListenLoop();
    
    Communication::Server* const GetServer() const;
    
private:
    const static char* DEFAULT_PORT;
    Communication::Server* _s;
};

#endif /* SERVER_H */


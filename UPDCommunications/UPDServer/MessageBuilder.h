/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MessageBuilder.h
 * Author: Samuel
 *
 * Created on February 9, 2016, 12:06 PM
 */

#ifndef MESSAGEBUILDER_H
#define MESSAGEBUILDER_H
#include <vector>

using namespace std;
class MessageBuilder {
public:
    MessageBuilder();
    MessageBuilder(int);
    MessageBuilder(const MessageBuilder& orig);
    virtual ~MessageBuilder();
    
    void addHeader();
    void addChar(char*, int);
    
    
private:
    struct Chunk{
        char* data;
        int length;
    };
    const static int CHUNK_SIZE;
    vector<Chunk> _chunks;
    Chunk* _current_working_chunk;
    
    void addNewChunk();
    void updateCurrentChunk(char*,int);
};

#endif /* MESSAGEBUILDER_H */


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: Samuel
 *
 * Created on March 13, 2016, 9:32 PM
 */

#include <cstdlib>
#include <iostream>
#include "Server.h"
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
	if (argc != 3) {
		cout << "Must put a value between 0 and 100 as first and second cmd argument " << endl;
		system("PAUSE");
		exit(2);
	}
	int lossRate = atoi(argv[1]);
	int delayRate = atoi(argv[2]);
    Server s;
    if (s.init(lossRate,delayRate,0)){
        s.mainListenLoop();
    }
    system("pause");
    return 0;
}


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Request.h
 * Author: Samuel
 *
 * Created on February 9, 2016, 3:44 PM
 */

#ifndef REQUEST_H
#define REQUEST_H
#include <string>

using namespace std;
class Request {
public:

	enum RequestType {
		UNEXPECTED_ERROR = 0,
		LIST_FILES = 1,
		PUT_FILE = 2,
		GET_FILE = 3,
		COMMAND_SUCCESFULL = 4,
		READY_TO_RECV = 5,
		READY_TO_SEND = 6,
		FILE_NOT_FOUND = 7,
		FILE_NOT_CREATED = 8,
		FILE_ALREADY_EXISTS = 9,
		RENAME_FILE = 10
    };

    Request();
    Request(RequestType,int);
    Request(RequestType,int,string);
    Request(const Request& orig);
    virtual ~Request();
    
    const unsigned char* getRawByteRepresentation();
    void fillFromRawByteRepresentation(const unsigned char*);
    
    RequestType getRequestType() const;
    int getSizeOfData() const;
    string getAddInfo() const;
    
    const static int REQUEST_SIZE;
    
private:
    void updateByteRepresentation();
    
    RequestType _req_type;
    int _size_of_data;
    string _add_info;
    unsigned char* _raw_data_representation;
};

#endif /* REQUEST_H */


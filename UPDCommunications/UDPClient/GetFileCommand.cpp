/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   GetFileCommand.cpp
 * Author: Samuel
 * 
 * Created on February 14, 2016, 3:11 PM
 */

#include "GetFileCommand.h"
#include "Request.h"
#define TEST
#include"..\UPDCommunications\FileIO.h"
#include <iostream>

GetFileCommand::GetFileCommand() {
}

GetFileCommand::GetFileCommand(const GetFileCommand& orig) {
}

GetFileCommand::~GetFileCommand() {
}

void GetFileCommand::cleanup() {
    _out_stream->close();
    delete _out_stream;
}

bool GetFileCommand::performDataTransfer() {

    int res = 0;
    int receivedData = 0;
    while (receivedData < _size_of_data) {
        const unsigned char* buffer = GetCommunicationEntity()->Recv();
        if (buffer == NULL) {
            return false;
        } else {
            _out_stream->write(reinterpret_cast<const char*>(buffer), GetCommunicationEntity()->GetLastPacketDataSize());
            receivedData += GetCommunicationEntity()->GetLastPacketDataSize();
        }
    }
    _out_stream->close();
    return true;

}

bool GetFileCommand::performInitialNego() {

    Request rr;
    const unsigned char* buffer = GetCommunicationEntity()->Recv();
    if (buffer == NULL) {
        return false;
    }
    rr.fillFromRawByteRepresentation(buffer);
    if (rr.getRequestType() != Request::READY_TO_SEND) {
        return false;
    }
    _size_of_data = rr.getSizeOfData();
    if (_ready_to_recv) {
        Request sr(Request::READY_TO_RECV, 0);
        if(!GetCommunicationEntity()->Send(sr.getRawByteRepresentation(),Request::REQUEST_SIZE)){
            return false;
        }
    }else{
        Request sr(Request::UNEXPECTED_ERROR, 0);
        GetCommunicationEntity()->Send(sr.getRawByteRepresentation(),Request::REQUEST_SIZE);
        return false;
    }

    return true;
}

bool GetFileCommand::sendCommand(string addInfo) {

    Request sr(Request::GET_FILE, 0, addInfo);
    if(!GetCommunicationEntity()->Send(sr.getRawByteRepresentation(), Request::REQUEST_SIZE)){
        return false;
    }
    Request rr;
    const unsigned char* buffer = GetCommunicationEntity()->Recv();
    
    if (buffer == NULL) {
        return false;
    }
    rr.fillFromRawByteRepresentation(buffer);
    if (rr.getRequestType() != Request::COMMAND_SUCCESFULL) {
        if (rr.getRequestType() == Request::FILE_NOT_FOUND) {
            cout << "The server did not find the file." << endl;
        }
        return false;
    }
    _ready_to_recv = true;
	string fnTemp = addInfo;
	while (FileIO::FileExists(fnTemp)) {
		fnTemp = FileIO::GetOverwriteName();
	}
	
	if (fnTemp.empty()) {
		_ready_to_recv = false;
	}
	else {
		_out_stream = new ofstream(fnTemp, ofstream::binary);
		if (!_out_stream->is_open()) {
			_ready_to_recv = false;
		}
	}
    

    return true;
}

void GetFileCommand::showResult() {
    std::cout << "File received succesfully" << endl;
}



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Client.h
 * Author: Samuel
 *
 * Created on February 9, 2016, 8:42 PM
 */

#ifndef CLIENT_HH
#define CLIENT_HH

#include <iostream>
#include "../UPDCommunications/Client.h"
//#include "../UDPCommunications/Client.h"

using namespace std;

class Client
{
private:
	const static char* DEFAULT_PORT;
	Communication::Client* _c;
public:
	Client();
	~Client();
	bool init(int, int, int);
	bool connect(string);
        
        Communication::Client* const GetClient() const;
        
};

#endif /* CLIENT_H */


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Client.cpp
 * Author: Samuel
 * 
 * Created on February 9, 2016, 8:42 PM
 */

#include "Client.h"

const char * Client::DEFAULT_PORT = "55555";

Client::Client() {
    //_data_socket = INVALID_SOCKET;
    _c = new Communication::Client();
    
}

Client::~Client() {
}

bool Client::init(int lossRate, int delayRate, int windowSize) {
//    WSAData wsaData;
//    int result;
//
//    cout << "Initialising WSA...";
//    result = WSAStartup(MAKEWORD(2, 2), &wsaData);
//    if (result != 0) {
//        cerr << "WSAStartup failed : " << result << endl;
//        return false;
//    } else {
//        cout << "OK" << endl;
//    }
//
//    return true;
	int realLossRate = (float)lossRate*255 / 100;
	int realDelayRate = (float)delayRate * 255 / 100;
    _c->InitializeLog("client.txt");
    return _c->Initialize("9999",false,realLossRate, realDelayRate, windowSize);
    
}

Communication::Client* const Client::GetClient() const {
    return _c;
}


bool Client::connect(string addr) {
//    int result;
//    addrinfo * addResult = NULL, hints;
//    ZeroMemory(&hints, sizeof (hints));
//    hints.ai_family = AF_INET;
//    hints.ai_socktype = SOCK_STREAM;
//    hints.ai_protocol = IPPROTO_TCP;
//
//    result = getaddrinfo(addr.c_str(), DEFAULT_PORT, &hints, &addResult);
//    if (result != 0) {
//        cerr << "getaddrinfo failed : " << result << endl;
//        WSACleanup();
//        return false;
//    }
//    
//    cout << "Trying to connect to " << addr << "...";
//    for (addrinfo* i = addResult; i != NULL; i = addResult->ai_next) {
//        _data_socket = socket(i->ai_family, i->ai_socktype, i->ai_protocol);
//        if (_data_socket == INVALID_SOCKET) {
//            cerr << "Invalid socket : " << WSAGetLastError << endl;
//            WSACleanup();
//            return false;
//        } else {
//            result = ::connect(_data_socket, i->ai_addr, (int) i->ai_addrlen);
//            if (result == SOCKET_ERROR) {
//                closesocket(_data_socket);
//                _data_socket = INVALID_SOCKET;
//                cout << "Connection failed..." << endl;
//            } else {
//                cout << "Connected." << endl;
//                return true;
//            }
//        }
//
//    }
//    return false;
    
    return _c->Connect(addr);
    
}



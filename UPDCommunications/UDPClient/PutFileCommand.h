/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   PutFileCommand.h
 * Author: Sam
 *
 * Created on February 10, 2016, 4:59 PM
 */

#ifndef PUTFILECOMMAND_H
#define PUTFILECOMMAND_H

#include "Command.h"
#include "FileChunker.h"


class PutFileCommand : public Command {
public:
    PutFileCommand();
    PutFileCommand(const PutFileCommand& orig);
    virtual ~PutFileCommand();
    
    void cleanup() override;
    bool performDataTransfer() override;
    bool performInitialNego() override;
    bool sendCommand(string) override;
    void showResult() override;
    
private:
    FileChunker* _file_chunker;
	double _initial_time;
	double _final_time;
};

#endif /* PUTFILECOMMAND_H */


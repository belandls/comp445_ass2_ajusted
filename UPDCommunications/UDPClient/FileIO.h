#pragma once
#include <Windows.h>
#include <tchar.h>
#include<iostream>
#define _EXPORT_ __declspec(dllexport)
namespace FileIO {

	_EXPORT_ bool FileExists(string& file) {
		WIN32_FIND_DATA FindFileData;
		HANDLE handle = FindFirstFile(file.c_str(), &FindFileData);
		int found = handle != INVALID_HANDLE_VALUE;
		if (found)
		{
			FindClose(handle);
		}
		return found;
	}

	_EXPORT_ string GetOverwriteName() {
		string input;
		std::cout << "There is already a file with that name. Please provide a new name (empty to cancel) : ";
		getline(cin, input);
		return input;
	}

}
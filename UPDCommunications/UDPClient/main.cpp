/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

 /*
  * File:   main.cpp
  * Author: Samuel
  *
  * Created on March 12, 2016, 2:44 PM
  */

#include <cstdlib>

#include "Client.h"
#include "ListFileCommand.h"
#include "FileChunker.h"
#include "PutFileCommand.h"
#include "GetFileCommand.h"
#include <vector>
#include <sstream>
#include <windows.h>
#include <tchar.h> 
#include <stdio.h>
#include <strsafe.h>
#include"RenameFileCommand.h"
#pragma comment(lib, "User32.lib")
using namespace std;

string getAddInfo(string q) {
	cout << q;
	string in;
	getline(cin, in);
	return in;
}

void listFiles() {
	cout << "Here are the local files : " << endl;
	WIN32_FIND_DATA ffd;
	LARGE_INTEGER filesize;
	TCHAR szDir[MAX_PATH];
	size_t length_of_arg;
	HANDLE hFind = INVALID_HANDLE_VALUE;
	DWORD dwError = 0;
	StringCchCopy(szDir, MAX_PATH, ".\\*");
	hFind = FindFirstFile(szDir, &ffd);
	do
	{
		if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			//_tprintf(TEXT("  %s   <DIR>\n"), ffd.cFileName);
		}
		else
		{
			filesize.LowPart = ffd.nFileSizeLow;
			filesize.HighPart = ffd.nFileSizeHigh;
			_tprintf(TEXT("  %s   %ld bytes\n"), ffd.cFileName, filesize.QuadPart);
		}
	} while (FindNextFile(hFind, &ffd) != 0);
	FindClose(hFind);
	//DIR *dir;
	//struct dirent *ent;
	//if ((dir = opendir(".")) != NULL) {
	//    /* print all the files and directories within directory */
	//    struct stat statbuf;
	//    while ((ent = readdir(dir)) != NULL) {
	//        if (stat(ent->d_name, &statbuf)) {
	//            continue;
	//        }
	//        if (S_ISDIR(statbuf.st_mode)) {
	//            continue;
	//        }
	//        cout << ent->d_name << endl;
	//    }
	//    closedir(dir);
	//} else {
	//    /* could not open directory */
	//    perror("");
	//}
}

/*
 *
 */
int main(int argc, char** argv) {
	if (argc != 4) {
		cout << "Must put a value between 0 and 100 as first and second cmd argument + the window size." << endl;
		system("PAUSE");
		exit(2);
	}
	int lossRate = atoi(argv[1]);
	int delayRate = atoi(argv[2]);
	int windowSize = atoi(argv[3]);

	Client c;
	if (c.init(lossRate, delayRate, windowSize)) {
		while (!c.GetClient()->IsConnected()) {
			
			c.GetClient()->ResetSeqNums();
			string input;
			cout << "Server address : ";
			getline(cin, input);

			if (c.connect(input)) {
				//            vector<Command*> commands;
				//            Command* temp = new GetFileCommand();
				//            temp->setWorkingSocket(c._data_socket);
				//            commands.push_back(temp);
				int res;
				do {
					cout << endl << "What do you want to do :" << endl;
					cout << "0. List Files (local)" << endl;
					cout << "1. List Files (server)" << endl;
					cout << "2. Put File" << endl;
					cout << "3. Get File" << endl;
					cout << "4. Rename File" << endl;
					cout << "5. Quit" << endl;

					res = 0;
					input.clear();
					cout << "Choice : ";
					getline(cin, input);
					stringstream tempStream(input);
					if (tempStream >> res) {
						if (res < 0 || res > 5) {
							cout << "Invalid choice." << endl;
							continue;
						}
					}
					else {
						cout << "The choice must be a number!" << endl;
						continue;
					}
					Command* selectedCommand = NULL;
					string addInfo;
					switch (res) {
					case 0:
						listFiles();
						continue;
						break;
					case 1:
						selectedCommand = new ListFileCommand();
						addInfo = "";
						break;
					case 2:
						selectedCommand = new PutFileCommand();
						addInfo = getAddInfo("What file do you want to upload? : ");
						break;
					case 3:
						selectedCommand = new GetFileCommand();
						addInfo = getAddInfo("What file do you want to download? : ");
						break;
					case 4:
						selectedCommand = new RenameFileCommand();
						addInfo = getAddInfo("What file do you want to rename? : ");
						break;
					case 5:
						continue;
					}
					cout << endl;
					selectedCommand->SetCommunicationEntity(c.GetClient());
					if (selectedCommand->sendCommand(addInfo)) {
						if (selectedCommand->performInitialNego()) {
							if (selectedCommand->performDataTransfer()) {
								selectedCommand->showResult();
							}
							else {
								cout << "There was an error transferring the data" << endl;
							}
							selectedCommand->cleanup();
						}
						else {
							cout << "There was an error during the handshake" << endl;
						}
					}
					else {
						cout << "There was an error sending the command" << endl;
					}
					cout << endl;
					delete selectedCommand;
				} while (res != 5 && c.GetClient()->IsConnected());
				if (!c.GetClient()->IsConnected()) {
					cout << "Disconnected" << endl;
				}
				system("pause");
			}
		}
	}

	return 0;
}


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   PutFileCommand.cpp
 * Author: Sam
 * 
 * Created on February 10, 2016, 4:59 PM
 */

#include "PutFileCommand.h"
#include "Request.h"
#include <iostream>
#include"TimeFunctions.h"
#define TEST
#include "..\UPDCommunications\FileIO.h"
using namespace std;

PutFileCommand::PutFileCommand() {
}

PutFileCommand::PutFileCommand(const PutFileCommand& orig) {
}

PutFileCommand::~PutFileCommand() {
}

void PutFileCommand::cleanup() {
    delete _file_chunker;
    _file_chunker = NULL;
}

bool PutFileCommand::performDataTransfer() {
	long totalSent = 0;
    while (!_file_chunker->isEOF()) {
        const char* data = _file_chunker->getNextChunk();
        int dataSize = _file_chunker->getLastChunkSize();
		if (dataSize == 0) {
			continue;
		}
        if(!GetCommunicationEntity()->Send(reinterpret_cast<const unsigned char*>(data), dataSize)){
            return false;
        }
		totalSent += dataSize;
		cout << "Queued for send : " << (int)(((float)totalSent / _file_chunker->getFileSize()) * 100) << "%";
		if (!_file_chunker->isEOF()) {
			cout << '\r';
		}
		else {
			cout << '\n';
		}
		cout << flush;
    }
	
	
    return true;
}

bool PutFileCommand::performInitialNego() {


    Request pr(Request::READY_TO_SEND, _file_chunker->getFileSize());
    if(!GetCommunicationEntity()->Send(pr.getRawByteRepresentation(),Request::REQUEST_SIZE)){
        return false;
    }

    const unsigned char* buffer = GetCommunicationEntity()->Recv();
    if (buffer == NULL) {
        return false;
    }
    Request rr;
    rr.fillFromRawByteRepresentation(buffer);
    if (rr.getRequestType() != Request::READY_TO_RECV) {
        return false;
    }
    return true;
}

bool PutFileCommand::sendCommand(string addInfo) {

	_initial_time = TimeFunctions::get_wall_time();
    _file_chunker = new FileChunker();

    if (_file_chunker->loadFile(addInfo)) {

        Request r(Request::PUT_FILE, 0, addInfo);
        if(!GetCommunicationEntity()->Send(r.getRawByteRepresentation(), Request::REQUEST_SIZE)){
            cerr << "Error while sending command" << endl;
            return false;
        }
        
        const unsigned char* buffer = GetCommunicationEntity()->Recv();
        if (buffer == NULL) {
            cerr << "Error while receiving answer" << endl;
            return false;
        }
        Request rr;
        rr.fillFromRawByteRepresentation(buffer);
		while (rr.getRequestType() == Request::FILE_ALREADY_EXISTS)
		{
			string fnTemp = FileIO::GetOverwriteName();
			Request r(Request::FILE_ALREADY_EXISTS, 0, fnTemp);
			if (!GetCommunicationEntity()->Send(r.getRawByteRepresentation(), Request::REQUEST_SIZE)) {
				cerr << "Error while sending command" << endl;
				return false;
			}
			
			buffer = GetCommunicationEntity()->Recv();
			rr.fillFromRawByteRepresentation(buffer);
			if (fnTemp.empty())
				break;
		}
        if (rr.getRequestType() != Request::COMMAND_SUCCESFULL) {
            if (rr.getRequestType() == Request::FILE_NOT_FOUND) {
                cout << "The server did not create the file." << endl;
            }
			if (rr.getRequestType() == Request::FILE_ALREADY_EXISTS) {
				cout << "There was a file name conflict." << endl;
			}
            return false;
        }
        return true;

    }
    return false;
}

void PutFileCommand::showResult() {
	cout << "Waiting for send buffer to empty...";
	GetCommunicationEntity()->WaitUntilSendBufferEmpties();
	_final_time = TimeFunctions::get_wall_time();
	cout << "Ok." << endl;
    cout << "File has been sent" << endl;
	cout << "Total elapsed time : ~" << _final_time - _initial_time << endl;
	GetCommunicationEntity()->DumpPacketStats();
}



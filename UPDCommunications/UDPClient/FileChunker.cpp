/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FileChunker.cpp
 * Author: Sam
 * 
 * Created on February 10, 2016, 12:18 PM
 */

#include "FileChunker.h"

const int FileChunker::CHUNK_MAX_SIZE = 1024;

int FileChunker::getFileSize() {
    return _file_size;
}

int FileChunker::getLastChunkSize() {
    return _last_chunk_size;
}

FileChunker::FileChunker() {
    _last_chunk_read = NULL;
    _last_chunk_size = 0;
    _file_size = 0;
    _in_stream = NULL;
    _is_eof = false;
    _is_loaded = false;
}

FileChunker::FileChunker(const FileChunker& orig) {
}

FileChunker::~FileChunker() {
}

const char* FileChunker::getNextChunk() {

    if (_is_loaded){
        if (!_is_eof){
            if (_last_chunk_read != NULL){
                delete[] _last_chunk_read;
                _last_chunk_read = NULL;
                _last_chunk_size =0;
            }
            _last_chunk_read = new char[CHUNK_MAX_SIZE];
            _in_stream->read(_last_chunk_read, CHUNK_MAX_SIZE);
            _last_chunk_size = _in_stream->gcount();
            if (*_in_stream){
                return _last_chunk_read;
            }else{
                //if (_last_chunk_size >= 0){
                    _is_eof = true;
                    return _last_chunk_read;
                //}
            }
        }
    }
    _last_chunk_size = 0;
    return NULL;
}

bool FileChunker::isEOF() {
    return _is_eof;
}

bool FileChunker::loadFile(string filename) {

    _in_stream = new ifstream(filename,ifstream::binary);
    if (_in_stream->is_open()){
        _in_stream->seekg(0, _in_stream->end);
        _file_size = _in_stream->tellg();
        _in_stream->seekg(0, _in_stream->beg);
        _is_loaded = true;
        return true;
    }
    return false;
}

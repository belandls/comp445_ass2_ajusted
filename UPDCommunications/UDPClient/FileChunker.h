/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FileChunker.h
 * Author: Sam
 *
 * Created on February 10, 2016, 12:18 PM
 */

#ifndef FILECHUNKER_H
#define FILECHUNKER_H
#include <string>
#include <fstream>
using namespace std;
class FileChunker {
public:
    FileChunker();
    FileChunker(const FileChunker& orig);
    virtual ~FileChunker();
    
    bool loadFile(string filename);
    const char* getNextChunk();
    int getLastChunkSize();
    bool isEOF();
    int getFileSize();
    const static int CHUNK_MAX_SIZE;
    
private:

    ifstream* _in_stream;
    int _file_size;
    bool _is_eof;
    bool _is_loaded;
    char* _last_chunk_read;
    int _last_chunk_size;
};

#endif /* FILECHUNKER_H */


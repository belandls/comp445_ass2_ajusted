#include "RenameFileCommand.h"
#include "..\UPDCommunications\FileIO.h"
#include "Request.h"


RenameFileCommand::RenameFileCommand()
{
}


RenameFileCommand::~RenameFileCommand()
{
}

bool RenameFileCommand::sendCommand(string addInfo)
{
	Request r(Request::RENAME_FILE, 0, addInfo);
	if (!GetCommunicationEntity()->Send(r.getRawByteRepresentation(), Request::REQUEST_SIZE)) {
		cerr << "Error while sending command" << endl;
		return false;
	}

	const unsigned char* buffer = GetCommunicationEntity()->Recv();
	if (buffer == NULL) {
		cerr << "Error while receiving answer" << endl;
		return false;
	}
	Request rr;
	rr.fillFromRawByteRepresentation(buffer);
	/*while (rr.getRequestType() == Request::FILE_ALREADY_EXISTS)
	{
		string fnTemp = FileIO::GetOverwriteName();
		Request r(Request::FILE_ALREADY_EXISTS, 0, fnTemp);
		if (!GetCommunicationEntity()->Send(r.getRawByteRepresentation(), Request::REQUEST_SIZE)) {
			cerr << "Error while sending command" << endl;
			return false;
		}

		buffer = GetCommunicationEntity()->Recv();
		rr.fillFromRawByteRepresentation(buffer);
		if (fnTemp.empty())
			break;
	}*/
	if (rr.getRequestType() != Request::COMMAND_SUCCESFULL) {
		if (rr.getRequestType() == Request::FILE_NOT_FOUND) {
			cout << "The server did not find the file." << endl;
		}
		return false;
	}
	return true;
}

bool RenameFileCommand::performInitialNego()
{
	string input;
	std::cout << "What is the new filename (empty to cancel) : ";
	getline(cin, input);

	Request r(Request::READY_TO_SEND, 0, input);
	if (!GetCommunicationEntity()->Send(r.getRawByteRepresentation(), Request::REQUEST_SIZE)) {
		cerr << "Error while sending command" << endl;
		return false;
	}

	const unsigned char* buffer = GetCommunicationEntity()->Recv();
	if (buffer == NULL) {
		cerr << "Error while receiving answer" << endl;
		return false;
	}

	Request rr;
	rr.fillFromRawByteRepresentation(buffer);
	while (rr.getRequestType() == Request::FILE_ALREADY_EXISTS)
	{
		input = FileIO::GetOverwriteName();
		Request r(Request::FILE_ALREADY_EXISTS, 0, input);
		if (!GetCommunicationEntity()->Send(r.getRawByteRepresentation(), Request::REQUEST_SIZE)) {
			cerr << "Error while sending command" << endl;
			return false;
		}

		buffer = GetCommunicationEntity()->Recv();
		rr.fillFromRawByteRepresentation(buffer);
		if (input.empty())
			break;
	}
	if (rr.getRequestType() != Request::READY_TO_RECV) {
		if (rr.getRequestType() == Request::FILE_ALREADY_EXISTS) {
			cout << "Operation canceled." << endl;
		}
		return false;
	}

	return true;
}

bool RenameFileCommand::performDataTransfer()
{
	return true;
}

void RenameFileCommand::showResult()
{
	cout << "File renamed." << endl;
}

void RenameFileCommand::cleanup()
{
}

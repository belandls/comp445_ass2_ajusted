/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ListFileCommand.cpp
 * Author: Samuel
 * 
 * Created on February 9, 2016, 8:47 PM
 */

#include "ListFileCommand.h"
#include "Request.h"
#include <iostream>

ListFileCommand::ListFileCommand() {
}

ListFileCommand::ListFileCommand(const ListFileCommand& orig) {
}

ListFileCommand::~ListFileCommand() {
}

bool ListFileCommand::performDataTransfer() {
//    int res = recv(getWorkingSocket(), _recv_buffer, _recv_buffer_size, 0);
    const unsigned char* res = GetCommunicationEntity()->Recv();
    _recv_buffer = new char[GetCommunicationEntity()->GetLastPacketDataSize()];
    memcpy(_recv_buffer, res, GetCommunicationEntity()->GetLastPacketDataSize());
	return true;
}

void ListFileCommand::showResult() {
    cout << "Here are the files on the server : " << endl;
    cout << _recv_buffer;
}

void ListFileCommand::cleanup() {
    delete[] _recv_buffer;
    _recv_buffer_size = 0;
}

bool ListFileCommand::performInitialNego() {
    
    const unsigned char* reqBuffer = GetCommunicationEntity()->Recv();
    Request r;
    
    r.fillFromRawByteRepresentation(reqBuffer);
    if (r.getRequestType() == Request::READY_TO_SEND) {

        _recv_buffer = new char[r.getSizeOfData()];
        _recv_buffer_size = r.getSizeOfData();

        Request sr(Request::READY_TO_RECV, 0);
        return GetCommunicationEntity()->Send(sr.getRawByteRepresentation(),Request::REQUEST_SIZE);
    } else {
        return false;
    }
    return true;
    
//    SOCKET currentSocket = getWorkingSocket();
//    int res;
//    char reqBuffer[260];
//
//    Request r;
//    res = recv(currentSocket, reqBuffer, 260, 0);
//    if (res == SOCKET_ERROR) {
//        return false;
//    }
//    r.fillFromRawByteRepresentation(reqBuffer);
//    if (r.getRequestType() == Request::READY_TO_SEND) {
//
//        _recv_buffer = new char[r.getSizeOfData()];
//        _recv_buffer_size = r.getSizeOfData();
//
//        Request sr(Request::READY_TO_RECV, 0);
//        res = send(currentSocket, sr.getRawByteRepresentation(), 260, 0);
//        if (res == SOCKET_ERROR) {
//            return false;
//        }
//    } else {
//        return false;
//    }
//    return true;

}

bool ListFileCommand::sendCommand(string addInfo) {
    Request r(Request::LIST_FILES, 0);
    GetCommunicationEntity()->Send(r.getRawByteRepresentation(), Request::REQUEST_SIZE);
    const unsigned char* buffer = GetCommunicationEntity()->Recv();
	if (buffer == NULL) {
		return false;
	}
    Request rr;
    rr.fillFromRawByteRepresentation(buffer);
    if (rr.getRequestType() != Request::COMMAND_SUCCESFULL) {
        return false;
    }
    return true;
}



#pragma once
#include "Command.h"
class RenameFileCommand :
	public Command
{
public:
	RenameFileCommand();
	virtual ~RenameFileCommand();



	// Inherited via Command
	virtual bool sendCommand(string) override;

	virtual bool performInitialNego() override;

	virtual bool performDataTransfer() override;

	virtual void showResult() override;

	virtual void cleanup() override;

};


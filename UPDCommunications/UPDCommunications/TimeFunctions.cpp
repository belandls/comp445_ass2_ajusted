/*
* File:   TimeFunctions.cpp
* Author: Samuel
* Description : Utility Functions
* Created on March 13, 2016, 4:34 PM
*/

#include "TimeFunctions.h"



TimeFunctions::TimeFunctions()
{
}


_EXPORT_ double TimeFunctions::get_wall_time()
{
	LARGE_INTEGER time, freq;
	if (!QueryPerformanceFrequency(&freq)) {
		return 0;
	}
	if (!QueryPerformanceCounter(&time)) {
		return 0;
	}
	return (double)time.QuadPart / freq.QuadPart;
}

TimeFunctions::~TimeFunctions()
{
}

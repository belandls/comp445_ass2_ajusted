/* 
 * File:   Server.cpp
 * Author: Samuel
 * Description : Handles the communication operations for a server
 * Created on March 13, 2016, 4:34 PM
 */

#include "Server.h"
#include <iostream>

using namespace Communication;

Server::Server() {
    _connected = false;
}

Server::Server(const Server& orig) {
}

Server::~Server() {
}

//Blocks until a valid hanshake is completed
void Server::AwaitConnection() {
    while (!GetReceiver().IsReceiving()) {
		GetReceiver().ResetSequenceNumbers();
		
        const Packet* p = GetReceiver().BlindRecv();
		Logger::Log << "Recv Packet from client... IsInit : " << p->IsInit() <<", IsAck : " << p->IsAck() << ", SeqNum :  " << (int)p->GetSeqNumber()<< '\n';
        if (p != NULL && p->IsInit() && !p->IsAck()) {
			GetReceiver().SetWindowSize(p->GetWindowSize());
            GetReceiver().SetReceivingSequenceNumber(p->GetSeqNumber());
			Logger::Log << "Initial Receiving SeqNum : " << (int)GetReceiver().GetReceivingSequenceNumber() << '\n';
            int initSequence = rand() % 255;
			Logger::Log << "Initial Sequence number : " << initSequence << '\n';
            Packet* p2 = new Packet(FLAG_ACK | FLAG_INIT, p->GetSeqNumber(), initSequence, true);
            p2->SetDestination(p->GetSource());
            if (GetReceiver().BlindSend(p2,true)) {
                cout << "Sent Ack" << endl;
				Logger::Log << "Sent Ack for " << (int)p->GetSeqNumber() << " with initSeq : " << initSequence << '\n';
                if (!GetReceiver().WaitForRecv(1000)) {
                    cout << "Waited 1 sec" << endl;
                    Packet* pErr = new Packet(FLAG_RESET, 255, 255, true);
					pErr->SetDestination(p->GetDestination());
					GetReceiver().BlindSend(pErr,true);
                    delete pErr;
                    cout << "Reset Sent" << endl;
                } else {
                    const Packet* p3 = GetReceiver().BlindRecv();
					Logger::Log <<"Received Packet from Client...IsInit : " << p3->IsInit() << ", IsAck : " << p3->IsAck() << ", AckNum :  " << (int)p3->GetAckNumber() << '\n';
                    if (p3 != NULL && p3->IsAck() && p3->IsInit() && p3->GetAckNumber() == initSequence) {
                        //_connected = true;
						GetReceiver().SetSendingSequenceNumber(p3->GetAckNumber());
						Logger::Log << "Initial Sending SeqNum : " << (int)GetReceiver().GetSendingSequenceNumber() << '\n';
						GetReceiver().SetConnectedDestination(p3->GetSource());
						GetReceiver().StartReceiving();
                    } else {
                        Packet* pErr = new Packet(FLAG_RESET, 255, 255, true);
						pErr->SetDestination(p->GetDestination());
						GetReceiver().BlindSend(pErr,true);
                        delete pErr;
                        cout << "Reset Sent" << endl;
                        cout << "Wrong ack received or ack not received" << endl;
                    }
                }
            } else {
                cout << "Error sending ACK" << endl;
            }
            delete p2;
		}
		else {
			Packet* pErr = new Packet(FLAG_RESET, 255, 255, true);
			pErr->SetDestination(p->GetDestination());
			GetReceiver().BlindSend(pErr, true);
			delete pErr;
		}
    }
}


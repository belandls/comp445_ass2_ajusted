/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Logger.cpp
 * Author: Sam
 * Description : Keeps a text file open to allow global logging.
 * Created on March 16, 2016, 11:42 AM
 */

#include "Logger.h"

Logger Logger::Log = Logger();
ofstream Logger::_out_file = ofstream();


Logger::Logger() { 
}

Logger::Logger(const Logger& orig) {
}

Logger::~Logger() {
}

//Initialized the text file for logging
bool Logger::Initialize(string logName)
{
	_out_file.open(logName.c_str(), std::ofstream::out | std::ofstream::trunc);
	if (_out_file.is_open()) {
		_out_file << "Starting a new session..." << endl;
		return true;
	}
	else {
		return false;
	}
}


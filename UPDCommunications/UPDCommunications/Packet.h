/* 
 * File:   Packet.h
 * Author: Samuel
 * Description : High-Level representation of a packet of data. This is what is sent between communication entities
 *				 Equivalent to a Tranport Layer Packet as Requests(see UDPClient/UDPServer) are equivalent to Application layer messages.
 * Created on March 13, 2016, 5:32 PM
 */
#define _WIN32_WINNT 0x0A00
#include<winsock2.h>
#include <ws2tcpip.h>

#pragma comment(lib, "Ws2_32.lib")

#define FLAG_ACK 1
#define FLAG_RESET 2	//When there was an error and the connection is not good anymore
#define FLAG_DATA 4
#define FLAG_INIT 8		//Designates a handshake packet
#define FLAG_NACK 16

#ifndef PACKET_H
#define PACKET_H

class Packet {
public:
    Packet(const char*, int, sockaddr_in);
	Packet(unsigned char);
    Packet(unsigned char, unsigned char, unsigned char);
    Packet(unsigned char, unsigned char, unsigned char,bool);
	Packet(unsigned char, unsigned char, unsigned char, unsigned char, bool);
    Packet(const Packet& orig);
    virtual ~Packet();
    
    static const int HEADER_SIZE = 8;
    
    int GetSizeOfPacket() const;
    
    void SetData(const unsigned char*, int);
    const unsigned char* GetRawRepresentation() const;
    void FillFromRawRepresentation(const unsigned char*, int);

    void SetDestination(addrinfo*);
    void SetDestination(sockaddr_in);

    sockaddr_in GetDestination() const;
    sockaddr_in GetSource() const;
    unsigned char GetAckNumber()const;
    unsigned char GetSeqNumber()const;
	unsigned char GetWindowSize() const;
    const unsigned char * GetData() const;
	void SetSeqNumber(unsigned char);
    bool IsAck() const;
	bool IsNack() const;
    bool IsInit() const;
    bool IsData() const;
private:
    int _size_of_data;							//Size of the data being sent (data only, no header)
    unsigned char _flags;						//Packet flags
    unsigned char _ack_number;					//Ack Number of packet, 255 if irrelevant in communication. Represented as a whole byte for simplicity during handshake. Normal tranfers only use the last bit for 1 or 0.
    unsigned char _seq_number;					//Seq Number of packet, 255 if irrelevant in communication. Represented as a whole byte for simplicity during handshake. Normal tranfers only use the last bit for 1 or 0.
    unsigned char* _packet_byte_representation;	//Raw byte representation of the Packet (Header+Data)
	unsigned char _window_size;
    sockaddr_in _source;						//Source of the Packet
    sockaddr_in _destination;					//Destination of the packet

	void UpdateHeader();
};

#endif /* PACKET_H */


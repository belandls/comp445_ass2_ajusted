#pragma once
#include <Windows.h>
#include <tchar.h>
#include<iostream>

namespace FileIO {

	static bool FileExists(string& file) {
		WIN32_FIND_DATA FindFileData;
		HANDLE handle = FindFirstFile(file.c_str(), &FindFileData);
		int found = handle != INVALID_HANDLE_VALUE;
		if (found)
		{
			FindClose(handle);
		}
		return found;
	}

	static string GetOverwriteName() {
		string input;
		std::cout << "There is already a file with that name. Please provide a new name (empty to cancel) : ";
		getline(cin, input);
		return input;
	}
	

	static bool RenameFile(string src, string dest) {
		return MoveFile(src.c_str(), dest.c_str());
	}

}
/* 
 * File:   Receiver.h
 * Author: Samuel
 * Description : Performs asynchronous Send/Receive of Packets using the Stop/Wait technique
 * Created on March 19, 2016, 7:57 AM
 */

#include"Packet.h"
#include<mutex>
#include<condition_variable>
#include<deque>
#include<string>
#include "Logger.h"
#include "TimeoutTimer.h"
#include <atomic>
using namespace std;

#ifndef RECEIVER_H
#define RECEIVER_H

class Receiver {
public:
    Receiver();
    Receiver(const Receiver& orig);
    virtual ~Receiver();
    
    const Packet* GetNextPacket();
    
	bool BlindSend(const Packet*, bool = false);
	const Packet* BlindRecv();
	bool Initialize(string, bool, short, short, int);
	bool WaitForRecv(long msec);
	void SetConnectedDestination(sockaddr_in);

	bool SetSequenceNumber(unsigned char&, unsigned char);
	bool SetReceivingSequenceNumber(unsigned char);
	bool SetSendingSequenceNumber(unsigned char);
	unsigned char GetReceivingSequenceNumber()const;
	unsigned char GetSendingSequenceNumber()const;
	unsigned char GetNextSendingSequenceNumber()const;
	string GetPort() const;
	void StartReceiving();
	void Send(Packet* p);
	void InitializeLog(string);
	bool IsReceiving() const;

	int GetPacketsSent()const;
	int GetPacketsQueued()const;
	int GetPacketsLost()const;
	int GetWindowSize() const;
	void ResetCounters();
	void SetWindowSize(int);
	void SetLossRate(short);
	void SetDelayRate(short);

	void WaitUntilSendBufferEmpties() const;
	
	void ResetSequenceNumbers();

private:

    deque<const Packet*> _recv_buffer;			//Buffered properly received packets ready to be consumed by a communication entuty
	deque<Packet*> _send_buffer;				//Buffered packets to send coming from a communication entity
	bool _data_available_to_receive;			//Marks there are unconsumed buffered received packets
	bool _data_available_to_send;				//Marks if data should be sent on the next send/receive loop
	mutex _sm;									//Sending mutex for ME during sending/buffering packets to send
	mutex _rm;									//Sending mutex for ME during sending/buffering packets received
	unsigned char _sending_sequence_number;		//Sequence number expected when a receiver
	unsigned char _receiving_sequence_number;	//Sequence number expected when a sender
	SOCKET _communication_socket;				//Single socket for raw communications
	string _port;								//Current server "listen" port
								//Packet loss rate
	sockaddr_in _connected_destination;			//Address/port of packets destination 
	thread* _t;									//Send/Recv thread.
	atomic<bool> _receiving;					//Marks if the thread is alive.
	deque<Packet*> _window;
	//For stats
	int _packets_sent;
	int _packets_queued;
	int _packets_lost;
	
	int _seq_num_range;
	int _last_sent_index;
	TimeoutTimer _timer;

	short _loss_rate;
	int _window_size;
	short _delay_rate;

    void Receive();
	void Disconnect();
	
};

#endif /* RECEIVER_H */


/* 
 * File:   CommunicationEntity.h
 * Author: Samuel
 * Description : Represents a communicating entity that can communicate with another communication entity (in that case, client with server)
 *  			 This is the DLL interface with outside code.
 * Created on March 13, 2016, 4:33 PM
 */


#include <string>
#include "Logger.h"
#include "Packet.h"
#include "Receiver.h"
using namespace std;

#ifndef COMMUNICATIONENTITY_H
#define COMMUNICATIONENTITY_H

#define _EXPORT_ __declspec(dllexport)
namespace Communication {

    class CommunicationEntity {
    public:
        CommunicationEntity();
        CommunicationEntity(const CommunicationEntity& orig);
        virtual ~CommunicationEntity();

        _EXPORT_ bool Initialize(string, bool, short,short, int);
        _EXPORT_ bool Send(const unsigned char*, int);
        _EXPORT_ const unsigned char* Recv();
        _EXPORT_ int GetLastPacketDataSize();
        _EXPORT_ void InitializeLog(string);
		_EXPORT_ bool IsConnected() const;
		_EXPORT_ void DumpPacketStats() const;
		_EXPORT_ void WaitUntilSendBufferEmpties() const;
		_EXPORT_ void SetWindowSize(int);
		_EXPORT_ void SetLossRateSize(short);
		_EXPORT_ void SetDelayRate(short);
		_EXPORT_ void ResetSeqNums();
    private:
        const Packet* _last_packet_received;	//The last packet that was Read. Used to get the size of the data.
		Receiver _r;							//Handles the asynchronous Send/Recv of Packets.

    protected:
        void SetConnectedDestination(sockaddr_in);
		Receiver& GetReceiver();
    };
}


#endif /* COMMUNICATIONENTITY_H */


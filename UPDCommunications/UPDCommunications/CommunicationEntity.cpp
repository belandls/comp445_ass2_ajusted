 /*
  * File:   CommunicationEntity.cpp
  * Author: Samuel
  * Description : Represents a communicating entity that can communicate with another communication entity (in that case, client with server)
  *  			  This is the DLL interface with outside code.
  * Created on March 13, 2016, 4:33 PM
  */

#include "CommunicationEntity.h"
#include <iostream>
#include <time.h>
#include <fstream>

using namespace Communication;

CommunicationEntity::CommunicationEntity() {
	_last_packet_received = NULL;
}

CommunicationEntity::CommunicationEntity(const CommunicationEntity& orig) {
}

CommunicationEntity::~CommunicationEntity() {
	delete _last_packet_received;
}

//Initializes the global logger.
void CommunicationEntity::InitializeLog(string filename) {
	Logger::Log.Initialize(filename);
}

//Check if this communication entity handshaked successfully with another comm. entity
_EXPORT_ bool Communication::CommunicationEntity::IsConnected() const
{
	return _r.IsReceiving();
}

_EXPORT_ void Communication::CommunicationEntity::DumpPacketStats() const
{
	int sent = _r.GetPacketsSent();
	int lost = _r.GetPacketsLost();
	int queued = _r.GetPacketsQueued();

	cout << "Total Packets Queued for send : " << queued << endl;
	cout << "Total Packets Sent (including resends) : " << sent << " (" << ((float)sent/queued) * 100 << "%)" << endl;
	cout << "Total Packets Lost : " << lost << " (" << ((float)lost / queued) * 100 << "%)" << endl;

	//char x[250];
	//sprintf(x, "%is.txt", _r._window_size);
	//ofstream os(x, ofstream::app | ofstream::out);
	//os << "(" << (int)((_r._delay_rate /(float)255)*100) << "," << sent << ")";
	//os.close();

	//sprintf(x, "%il.txt", _r._window_size);
	//ofstream os2(x, ofstream::app | ofstream::out);
	//os2 << "(" << (int)((_r._delay_rate / (float)255) * 100) << "," << lost << ")";
	//os2.close();

	const_cast<Receiver&>(_r).ResetCounters();
}

//Sync. this thread with the sending of packet.
_EXPORT_ void Communication::CommunicationEntity::WaitUntilSendBufferEmpties() const
{
	_r.WaitUntilSendBufferEmpties();
}

_EXPORT_ void Communication::CommunicationEntity::SetWindowSize(int ws)
{
	_r.SetWindowSize(ws);
}

_EXPORT_ void Communication::CommunicationEntity::SetLossRateSize(short lr)
{
	_r.SetLossRate(lr);
}

_EXPORT_ void Communication::CommunicationEntity::SetDelayRate(short dr)
{
	_r.SetDelayRate(dr);
}

_EXPORT_ void Communication::CommunicationEntity::ResetSeqNums()
{
	_r.ResetSequenceNumbers();
}

//Initializes the communications essentials (socket, etc.)
bool CommunicationEntity::Initialize(string port, bool bindSocket, short lossRate, short delayRate, int windowSize) {
	return _r.Initialize(port, bindSocket, lossRate, delayRate, windowSize);
}

//Get the raw data from the next packet that was received.
const unsigned char* CommunicationEntity::Recv() {
	const Packet* tempPack = _r.GetNextPacket(); //This is blocking if no packets were received
	if (tempPack != NULL) {
		_last_packet_received = tempPack;
		return tempPack->GetData();
	}
	return NULL;
}

int CommunicationEntity::GetLastPacketDataSize() {
	if (_last_packet_received == NULL) {
		return -1;
	}
	return _last_packet_received->GetSizeOfPacket() - Packet::HEADER_SIZE;
}

//Send Raw Data to the other connected communication entity
bool CommunicationEntity::Send(const unsigned char* data, int dataSize) {
	Packet* p = new Packet(FLAG_DATA);
	p->SetData(data, dataSize);
	_r.Send(p); //Queue the packet for sending
	return true;
}

//Keep track of the add./Port of the other communication entity
void CommunicationEntity::SetConnectedDestination(sockaddr_in dest) {
	_r.SetConnectedDestination(dest);
}

Receiver & Communication::CommunicationEntity::GetReceiver()
{
	return _r;
}


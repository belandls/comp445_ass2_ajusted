#include "TimeoutTimer.h"



void TimeoutTimer::StartTimer()
{
	while (_is_alive) {
		if (_is_running && !_timeout) {
			auto elapsed = chrono::high_resolution_clock::now() - _start_time;
			if (elapsed >= _interval && _is_running) {
				_timeout = true;
			}
			this_thread::yield();
		}
		else {
			this_thread::sleep_for(5ms);
		}
	}
}

TimeoutTimer::TimeoutTimer()
{
	_is_alive = true;
	t = thread(&TimeoutTimer::StartTimer, this);
}


TimeoutTimer::~TimeoutTimer()
{
}

bool TimeoutTimer::HasTimedOut() const
{
	return _timeout;
}

void TimeoutTimer::Start(chrono::nanoseconds interval)
{
	_interval = interval;
	_timeout = false;
	_start_time = chrono::high_resolution_clock::now();
	_is_running = true;
}

void TimeoutTimer::Restart()
{
	_is_running = false;
	_timeout = false;
	_start_time = chrono::high_resolution_clock::now();
	_is_running = true;
}

void TimeoutTimer::Stop()
{
	_is_running = false;
}

#pragma once
#include "Packet.h"
#include <string>
using namespace std;
class RawSendRecv
{
public:
	RawSendRecv();
	~RawSendRecv();

	bool BlindSend(Packet*);
	const Packet* BlindRecv();
	bool Initialize(string, bool, short);
	bool WaitForRecv(long msec);
	void SetConnectedDestination(sockaddr_in);

private:
	SOCKET _communication_socket;
	string _port;
	short _loss_rate;
	sockaddr_in _connected_destination;
};


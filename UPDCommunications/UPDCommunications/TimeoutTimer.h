#pragma once
#include <chrono>
#include <thread>
using namespace std;
class TimeoutTimer
{
private:
	bool _is_alive;
	bool _timeout;
	bool _is_running;
	chrono::nanoseconds _interval;
	chrono::time_point<chrono::high_resolution_clock> _start_time;
	thread t;

	void StartTimer();

public:
	TimeoutTimer();
	~TimeoutTimer();

	bool HasTimedOut() const;
	void Start(chrono::nanoseconds interval);
	void Restart();
	void Stop();
};


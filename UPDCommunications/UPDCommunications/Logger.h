/* 
 * File:   Logger.h
 * Author: Sam
 * Description : Keeps a text file open to allow global logging.
 * Created on March 16, 2016, 11:42 AM
 */

#include<fstream>
#include<string>
using namespace std;
#ifndef LOGGER_H
#define LOGGER_H

class Logger {
public:
    
    Logger(const Logger& orig);
    virtual ~Logger();

	static Logger Log;
	static bool Initialize(string);

    template<typename T>
    friend Logger& operator<<(Logger & log,  T const& obj);

private:
	Logger();
    //bool _loaded;
    static ofstream _out_file;
};

template<typename T>
Logger& operator<<(Logger& log, T const& obj) {
    log._out_file << obj;
	log._out_file.flush();
    return log;
}

#endif /* LOGGER_H */


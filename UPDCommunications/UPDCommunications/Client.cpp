
/* 
 * File:   Client.cpp
 * Author: Samuel
 * Description : Handles the communication operations for a client
 * Created on March 13, 2016, 4:34 PM
 */

#include "Client.h"
#include<iostream>
using namespace std;
using namespace Communication;

Client::Client() {

}

Client::Client(const Client& orig) {
}

Client::~Client() {
}

//Connect the a Server using the 3-way handshake and returns True if connected.
bool Client::Connect(string addr) {

    int result;
    addrinfo * addResult = NULL, hints;
    ZeroMemory(&hints, sizeof (hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_protocol = IPPROTO_UDP;

	//Performs DNS request.
    result = getaddrinfo(addr.c_str(), GetReceiver().GetPort().c_str(), &hints, &addResult);
    if (result != 0) {
        cerr << "getaddrinfo failed : " << result << endl;
        //WSACleanup();
        return false;
    }
    int initSequence = rand() % 255;
	Logger::Log << "Initial Sequence number : " << initSequence << '\n';
    Packet* p = new Packet(FLAG_INIT, 0, initSequence, GetReceiver().GetWindowSize(),  true);
    p->SetDestination(addResult);
    if (!GetReceiver().BlindSend(p,true)) {
        return false;
    }
	Logger::Log << "Sent Init Sequence to Server...\n";
    if (GetReceiver().WaitForRecv(1000)) {
        const Packet *p2 = GetReceiver().BlindRecv();
        if (p2 != NULL && p2->IsAck() && p2->IsInit() && p2->GetAckNumber() == initSequence) {
			Logger::Log << "Recv Packet From Server... IsAck : " << p2->IsAck() << ", IsInit : " << p2->IsInit() << ", SeqNum : " << (int)p2->GetSeqNumber() << ", AckNum : " << (int)p2->GetAckNumber() << '\n';
            GetReceiver().SetSendingSequenceNumber(p2->GetAckNumber());
            GetReceiver().SetReceivingSequenceNumber(p2->GetSeqNumber());
			Logger::Log << "Initial SendingSeqNumber : " << (int)GetReceiver().GetSendingSequenceNumber() << ", Initial RecvSeqNumber : " << (int)GetReceiver().GetReceivingSequenceNumber() <<'\n';
            Packet* p3 = new Packet(FLAG_INIT | FLAG_ACK, p2->GetSeqNumber(), 0, true);
            p3->SetDestination(addResult);
			Logger::Log << "Ack'ing seq num : " << (int)p3->GetAckNumber() << '\n';
            if (!GetReceiver().BlindSend(p3, true)) {
                delete p3;
                return false;
            } else {
				GetReceiver().SetConnectedDestination(p3->GetDestination());
				GetReceiver().StartReceiving();
                delete p3;
                return true;
            }

        } else {
            return false;
        }
    } else {
        Packet* pErr = new Packet(FLAG_RESET, 255, 255, true);
		GetReceiver().BlindSend(pErr, true);
        delete pErr;
        return false;
    }

    freeaddrinfo(addResult);
    delete p;
    return false;
}



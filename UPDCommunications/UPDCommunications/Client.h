/* 
 * File:   Client.h
 * Author: Samuel
 * Description : Handles the communication operations for a client
 * Created on March 13, 2016, 4:34 PM
 */


#ifndef CLIENT_H
#define CLIENT_H
#define _EXPORT_ __declspec(dllexport)

#include "CommunicationEntity.h"


namespace  Communication {

    class Client : public CommunicationEntity {
    public:
        _EXPORT_ Client();
        _EXPORT_ Client(const Client& orig);
        _EXPORT_ virtual ~Client();

        _EXPORT_ bool Connect(string);

    private:

    };
}


#endif /* CLIENT_H */


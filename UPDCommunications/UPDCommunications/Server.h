/* 
 * File:   Server.h
 * Author: Samuel
 * Description : Handles the communication operations for a server
 * Created on March 13, 2016, 4:34 PM
 */

#ifndef SERVER_H
#define SERVER_H

#include "CommunicationEntity.h"

namespace Communication {

    class Server : public CommunicationEntity {
    public:
        _EXPORT_ Server();
        _EXPORT_ Server(const Server& orig);
        _EXPORT_ virtual ~Server();

        _EXPORT_ void AwaitConnection();

    private:
        bool _connected;
    };
}


#endif /* SERVER_H */


/*
* File:   TimeFunctions.cpp
* Author: Samuel
* Description : Utility Functions
* Created on March 13, 2016, 4:34 PM
*/

#pragma once
#define _EXPORT_ __declspec(dllexport)
#include <Windows.h>
class TimeFunctions
{
public:
	
	_EXPORT_ static double get_wall_time();

	~TimeFunctions();
private:
	TimeFunctions();
};


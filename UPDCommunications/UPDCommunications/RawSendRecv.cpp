#include "RawSendRecv.h"
#include<time.h>
#include <iostream>


RawSendRecv::RawSendRecv()
{
}


RawSendRecv::~RawSendRecv()
{
	closesocket(_communication_socket);
	WSACleanup();
}

bool RawSendRecv::Initialize(string port, bool bindSocket, short lossRate) {
	srand(time(NULL));
	_loss_rate = lossRate;
	int res;
	WSAData wsaData;
	addrinfo * addResult = NULL, hints;

	cout << "Initialising WSA...";
	res = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (res != 0) {
		cerr << "WSAStartup failed : " << res << endl;
		return false;
	}
	else {
		cout << "OK" << endl;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_protocol = IPPROTO_UDP;
	hints.ai_flags = AI_PASSIVE;

	res = getaddrinfo(NULL, port.c_str(), &hints, &addResult);
	if (res != 0) {
		cerr << "getaddrinfo failed : " << res << endl;
		WSACleanup();
		return false;
	}

	cout << "Creating communication socket...";
	_communication_socket = socket(addResult->ai_family, addResult->ai_socktype, addResult->ai_protocol);
	if (_communication_socket == INVALID_SOCKET) {
		cerr << "Socket creation Failed : " << WSAGetLastError() << endl;
		freeaddrinfo(addResult);
		WSACleanup();
		return false;
	}
	else {
		cout << "OK." << endl;
	}

	if (bindSocket) {
		cout << "Binding socket...";
		res = bind(_communication_socket, addResult->ai_addr, (int)addResult->ai_addrlen);
		if (res == SOCKET_ERROR) {
			cerr << "Socket binding Failed : " << WSAGetLastError() << endl;
			freeaddrinfo(addResult);
			closesocket(_communication_socket);
			WSACleanup();
			return false;
		}
		else {
			cout << "Ok." << endl;
		}
	}
	freeaddrinfo(addResult);

	_port = port;

	return true;

}

const Packet* RawSendRecv::BlindRecv() {

	char buffer[2048];
	sockaddr_in SenderAddr;
	Packet* temp;
	int SenderAddrSize = sizeof(SenderAddr);

	int res = recvfrom(_communication_socket, buffer, 2048, 0, (SOCKADDR *)& SenderAddr, &SenderAddrSize);

	if (res == 0 || res == SOCKET_ERROR) {
		temp = NULL;
	}
	else {
		temp = new Packet(buffer, res, SenderAddr);
	}

	return temp;
}

bool RawSendRecv::BlindSend(Packet* p) {	
	int res;
	if (rand() % 256 < _loss_rate) {
		res = 1;
	}
	else {
		res = sendto(_communication_socket, reinterpret_cast<const char*> (p->GetRawRepresentation()), p->GetSizeOfPacket(), 0, (sockaddr*)& _connected_destination, sizeof(_connected_destination));
	}
	if (res == 0 || res == SOCKET_ERROR) {
		return false;
	}
	else {
		return true;
	}

}

bool RawSendRecv::WaitForRecv(long msec) {
	fd_set recvSet;
	FD_ZERO(&recvSet);
	FD_SET(_communication_socket, &recvSet);
	timeval to;
	to.tv_sec = msec / 1000;
	to.tv_usec = (msec % 1000) * 1000;
	int res = select(0, &recvSet, NULL, NULL, &to);
	if (res == -1) {
		cout << WSAGetLastError() << endl;
	}
	return res > 0;
}

void RawSendRecv::SetConnectedDestination(sockaddr_in dest) {
	_connected_destination = dest;
}
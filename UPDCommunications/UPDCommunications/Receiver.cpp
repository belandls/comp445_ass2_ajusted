 /*
  * File:   Receiver.cpp
  * Author: Samuel
  * Description : Performs asynchronous Send/Receive of Packets using the Stop/Wait technique
  * Created on March 19, 2016, 7:57 AM
  */

#include "Receiver.h"
#include <thread>
#include <iostream>
using namespace std;

Receiver::Receiver() {
	_sending_sequence_number = 255;
	_receiving_sequence_number = 255;
	ResetCounters();
	_window_size = 15;
	_seq_num_range = _window_size * 2;
	_last_sent_index = -1;
}

Receiver::Receiver(const Receiver& orig) {
}

Receiver::~Receiver() {
}

//Get the next received packet
const Packet* Receiver::GetNextPacket() {

	//Make the call blocking while no data is received
	while (!_data_available_to_receive && _receiving) {
		this_thread::sleep_for(1ms);
		this_thread::yield();
	}
	//If there was a connection problem
	if (!_receiving) {
		Disconnect();
		return NULL;
	}
	//returns the next buffered packet
	unique_lock<mutex> l(_rm);
	const Packet* temp = _recv_buffer.front();
	_recv_buffer.pop_front();
	_data_available_to_receive = !_recv_buffer.empty();
	return temp;
}

//Main infinite loop runned in an independent thread
void Receiver::Receive() {
	bool shouldRecv = true;		//Marks if any error occured
	bool ackPending = false;	//Marks if an ack should be received
	int retries = 0;
	while (shouldRecv) {

		if (_data_available_to_send) {
			while (_window.size() != _window_size && _send_buffer.size() != 0) {
				unique_lock<mutex> sl(_sm);
				Packet* temp = _send_buffer.front();
				temp->SetSeqNumber((_sending_sequence_number + _window.size()) % _seq_num_range);
				Logger::Log << "Adding packet " << _sending_sequence_number + _window.size() << " to the window\n";
				_window.push_back(temp);
				_send_buffer.pop_front();
			}
			_data_available_to_send = !_send_buffer.empty() || !_window.empty();
			int baseIndex;
			if (_timer.HasTimedOut()) {
				baseIndex = 0;
				_last_sent_index = -1;
				Logger::Log << "Timed out...\n";
				++_packets_lost;
				if (++retries > 20) {
					shouldRecv = false;
					break;
				}
			}
			else {
				baseIndex = _last_sent_index + 1;
				//Logger::Log << "LastIndex : " << _last_sent_index << ", WindowSize : " << _window.size() << '\n';
			}
			Packet* delayed = NULL;
			for (int i = baseIndex; i < _window.size(); ++i, ++_last_sent_index) {
				if (delayed != NULL) {
					Logger::Log << "Sending packet " << (int)delayed->GetSeqNumber() << '\n';
					BlindSend(delayed);
					delayed = NULL;
				}
				else {
					if (i < _window.size() - 1) {
						if (rand() % 256 < _delay_rate) {
							delayed = _window[i];
							Logger::Log << "Sending packet " << (int)_window[i+1]->GetSeqNumber() << '\n';
							BlindSend(_window[i + 1]);
						}
						else {
							Logger::Log << "Sending packet " << (int)_window[i]->GetSeqNumber() << '\n';
							BlindSend(_window[i]);
						}
					}
					else {
						Logger::Log << "Sending packet " << (int)_window[i]->GetSeqNumber() << '\n';
						BlindSend(_window[i]);
					}
				}
				
				++_packets_sent;
				
				if (_last_sent_index == -1) {
					_timer.Start(200ms);
				}
			}
		}

		//******************************************************************************
		//Replace by while to prioritize receiving
		//******************************************************************************
		while (WaitForRecv(50)) {
			/*if (_timer.HasTimedOut())
				continue;*/
			const Packet* recvPack = BlindRecv();
			if (recvPack != NULL && recvPack->IsAck()) {
				if (SetSendingSequenceNumber(recvPack->GetAckNumber() + 1)) {
					_timer.Stop();
					retries = 0;
					Logger::Log << "Received ACK for packet " << (int)recvPack->GetAckNumber() << '\n';
					_window.pop_front();
					if (--_last_sent_index >= 0) {
						Logger::Log << "Restarting...\n";
						_timer.Restart();
					}
				}
				else {
					Logger::Log << "Well that happened\n";
				}
			}
			else if (recvPack != NULL && recvPack->IsNack()) {
				if (recvPack->GetAckNumber() == _sending_sequence_number) {
					_timer.Stop();
					Logger::Log << "Received NACK for packet " << (int)recvPack->GetAckNumber() << ". Duplicate, so skipping.\n";
					_window.pop_front();
					if (--_last_sent_index >= 0) {
						_timer.Restart();
					}
				}
				else {
					Logger::Log << "***ERROR** Received NACK for packet " << (int)recvPack->GetAckNumber() << '\n';
				}
				
			}
			else if (recvPack != NULL && recvPack->IsData()) {
				//Buffer the packet if not a duplicate
				if (SetReceivingSequenceNumber(recvPack->GetSeqNumber() + 1)) {
					Logger::Log << "Received packet " << (int)recvPack->GetSeqNumber() << "...";
					Packet* ackPacket = new Packet(FLAG_ACK, recvPack->GetSeqNumber(), 255, true);
					BlindSend(ackPacket);
					delete ackPacket;
					Logger::Log << " Sent ACK\n";
					unique_lock<mutex> rl(_rm);
					_recv_buffer.push_back(recvPack);
					_data_available_to_receive = true;
				}
				else {
					Logger::Log << "Received packet " << (int)recvPack->GetSeqNumber() << "...";
					Packet* ackPacket = new Packet(FLAG_NACK, recvPack->GetSeqNumber(), 255, true);
					BlindSend(ackPacket);
					delete ackPacket;
					Logger::Log << " Sent NACK, out of order\n";
				}
			}
			else {
				Logger::Log << "wut? \n\n";
			}

		}
		//The loop is simple. 
		//while(true)
		//	Send if there is a packet to send
		//	If ackPending, wait for recv until timeout
		//	If data received, treat it.
		//repeate

		/*if (_data_available_to_send) { //Will stay true until all acks are received
			if (ackPending) { //This means we timed out before receiving the ACK
				++_packets_lost;
				if (++retries > 20) {
					shouldRecv = false;
					break;
				}
				Logger::Log << "Resending Packet (" << (int)_sending_sequence_number << "), retry #" << retries << '\n';
			}
			++_packets_sent;
			Packet* tempPack = _send_buffer.front();
			tempPack->SetSeqNumber(_sending_sequence_number);
			if (BlindSend(tempPack)) {
				Logger::Log << "Sent Data with sequence number " << (int)_sending_sequence_number << "\n";
				ackPending = true;
			}
			else {
				shouldRecv = false;
				break;
			}
		}
		long timeoutVal;
		//If we are waiting for an ack, wait 500ms, else wait at least 50ms simply to avoid the thread abusively looping and hogging the CPU
		if (ackPending) {
			timeoutVal = 500;
		}
		else {
			timeoutVal = 50;
		}
		if (WaitForRecv(timeoutVal)) {
			const Packet* recvPack = BlindRecv();
			Logger::Log << "Recv...";
			//If we received an ACK
			if (recvPack != NULL && recvPack->IsAck()) {
				Logger::Log << "Ack(" << (int)recvPack->GetAckNumber() << "): ";
				if (SetSendingSequenceNumber(recvPack->GetAckNumber() + 1)) {
					//Remove packet from send buffer
					unique_lock<mutex> sl(_sm);
					_send_buffer.pop_front();
					_data_available_to_send = !_send_buffer.empty();
					ackPending = false;
					retries = 0;
					sl.unlock();
					Logger::Log << "OK\n";
				}
				else {
					Logger::Log << "Wrong ACK Number\n";
				}
			}
			//If it's data
			else if(recvPack != NULL && recvPack->IsData()) {
				//Send an ack
				Logger::Log << "Data:";
				Packet* ackPacket = new Packet(FLAG_ACK, recvPack->GetSeqNumber(), 255, true);
				BlindSend(ackPacket);
				delete ackPacket;
				//Buffer the packet if not a duplicate
				if (SetReceivingSequenceNumber(recvPack->GetSeqNumber() + 1)) {
					unique_lock<mutex> rl(_rm);
					_recv_buffer.push_back(recvPack);
					_data_available_to_receive = true;
					Logger::Log << "Ack Sent(" << (int)recvPack->GetSeqNumber() << "), Sequence number updated (" << (int)_receiving_sequence_number << ").\n";
				}
				else {
					Logger::Log << "Ack Sent(" << (int)recvPack->GetSeqNumber() << "), Data ignored.\n";
				}
			}
			else {
				shouldRecv = false;
				break;
			}
		}
		else {
			if (ackPending) {
				Logger::Log << "Timeout on ACK for : " << (int)_sending_sequence_number << "\n";
			}
		}*/
	}
	_receiving = false;
}


//Initialize Winsock and the communication socket
bool Receiver::Initialize(string port, bool bindSocket, short lossRate, short delayRate, int windowSize) {
	srand(time(NULL));
	SetWindowSize(windowSize);
	_loss_rate = lossRate;
	_delay_rate = delayRate;
	int res;
	WSAData wsaData;
	addrinfo * addResult = NULL, hints;

	cout << "Initialising WSA...";
	res = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (res != 0) {
		cerr << "WSAStartup failed : " << res << endl;
		return false;
	}
	else {
		cout << "OK" << endl;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_protocol = IPPROTO_UDP;
	hints.ai_flags = AI_PASSIVE;

	res = getaddrinfo(NULL, port.c_str(), &hints, &addResult);
	if (res != 0) {
		cerr << "getaddrinfo failed : " << res << endl;
		WSACleanup();
		return false;
	}

	cout << "Creating communication socket...";
	_communication_socket = socket(addResult->ai_family, addResult->ai_socktype, addResult->ai_protocol);
	if (_communication_socket == INVALID_SOCKET) {
		cerr << "Socket creation Failed : " << WSAGetLastError() << endl;
		freeaddrinfo(addResult);
		WSACleanup();
		return false;
	}
	else {
		cout << "OK." << endl;
	}

	if (bindSocket) {
		cout << "Binding socket...";
		res = ::bind(_communication_socket, addResult->ai_addr, (int)addResult->ai_addrlen);

		if (res == SOCKET_ERROR) {
			cerr << "Socket binding Failed : " << WSAGetLastError() << endl;
			freeaddrinfo(addResult);
			closesocket(_communication_socket);
			WSACleanup();
			return false;
		}
		else {
			cout << "Ok." << endl;
		}
	}
	freeaddrinfo(addResult);

	_port = port;

	return true;

}

//Blindly tries to receive from communication socket (no Stop/Wait, raw recvfrom)
const Packet* Receiver::BlindRecv() {

	char buffer[2048];
	sockaddr_in SenderAddr;
	Packet* temp;
	int SenderAddrSize = sizeof(SenderAddr);

	int res = recvfrom(_communication_socket, buffer, 2048, 0, (SOCKADDR *)& SenderAddr, &SenderAddrSize);

	if (res == 0 || res == SOCKET_ERROR) {
		Logger::Log << WSAGetLastError() << '\n';
		temp = NULL;
	}
	else {
		temp = new Packet(buffer, res, SenderAddr);
	}

	return temp;
}

//Blindly tries to send from communication socket (no Stop/Wait, raw sendto)
bool Receiver::BlindSend(const Packet* p, bool usePacketDestination) {
	sockaddr_in dest;
	if (usePacketDestination) {
		dest = p->GetDestination();
	}
	else {
		dest = _connected_destination;
	}
	int res;
	//To fake a lost packet, I randomly trick a valid "send" even though nothing is sent. 
	if (rand() % 256 < _loss_rate) {
		Logger::Log << "Packet Lost\n";
		res = 1; //Dummy value to return true
	}
	else {
		res = sendto(_communication_socket, reinterpret_cast<const char*> (p->GetRawRepresentation()), p->GetSizeOfPacket(), 0, (sockaddr*)& dest, sizeof(dest));
		//++_packets_sent;
	}
	if (res == 0 || res == SOCKET_ERROR) {
		return false;
	}
	else {
		return true;
	}

}

//Checks if a packet was received, else timeout after msec and return false
bool Receiver::WaitForRecv(long msec) {
	fd_set recvSet;
	FD_ZERO(&recvSet);
	FD_SET(_communication_socket, &recvSet);
	timeval to;
	to.tv_sec = msec / 1000;
	to.tv_usec = (msec % 1000) * 1000;
	int res = select(0, &recvSet, NULL, NULL, &to);
	if (res == -1) {
		cout << WSAGetLastError() << endl;
	}
	return res > 0;
}

void Receiver::SetConnectedDestination(sockaddr_in dest) {
	_connected_destination = dest;
}

unsigned char Receiver::GetNextSendingSequenceNumber() const {
	if (_sending_sequence_number == 255) {
		return 255;
	}
	else {
		return (_sending_sequence_number + 1) % 2;
	}
}

string Receiver::GetPort() const
{
	return _port;
}

void Receiver::StartReceiving()
{
	_t = new thread(&Receiver::Receive, this);
	_receiving = true;
}

//Buffers a packet for async send
void Receiver::Send(Packet * p)
{
	if (!_receiving) {
		Disconnect();
		return;
	}
	//Blocks in case too much packet are buffered at once
	while (_send_buffer.size() > _window_size*2) {
		this_thread::sleep_for(1ms);
		this_thread::yield();
	}
	unique_lock<mutex> l(_sm);
	_send_buffer.push_back(p);
	_data_available_to_send = !_send_buffer.empty();
	++_packets_queued;
}
void Receiver::Disconnect()
{
	if (_t != NULL) {
		_t->join();
		delete _t;
		_t = NULL;
	}
	_send_buffer.clear();
	_recv_buffer.clear();
	_data_available_to_receive = false;
	_data_available_to_send = false;
}

unsigned char Receiver::GetReceivingSequenceNumber() const {
	return _receiving_sequence_number;
}

unsigned char Receiver::GetSendingSequenceNumber() const {
	return _sending_sequence_number;
}

bool Receiver::SetSendingSequenceNumber(unsigned char sendSeq) {
	int temp = sendSeq % _seq_num_range;
	if (_sending_sequence_number == 255) {
		_sending_sequence_number = temp;
	}
	else {
		int upper = 0;
		int lower = 0;
		if (_sending_sequence_number + _window_size >= _seq_num_range) {
			upper = _sending_sequence_number;
			lower = (_sending_sequence_number + _window_size) % _seq_num_range;
		}
		else {
			upper = (_sending_sequence_number + _window_size) % _seq_num_range;
			lower = _sending_sequence_number;
		}


		if (_sending_sequence_number + _window_size >= _seq_num_range) {
			if (temp > lower && temp < upper) {
				Logger::Log << "Nothing1\n";
			}
			else {
				_sending_sequence_number = temp;
			}
		}
		else {
			if (temp > lower && temp < upper) {
				_sending_sequence_number = temp;
			}
			else {
				Logger::Log << "Nothing2\n";
				//nothing
			}
		}
	}
	
	return true;
	//return SetSequenceNumber(_sending_sequence_number, sendSeq);
}

bool Receiver::SetReceivingSequenceNumber(unsigned char recSeq) {
	unsigned char realLastSequence = recSeq % _seq_num_range;
	if ((_receiving_sequence_number + 1) % _seq_num_range == realLastSequence || _receiving_sequence_number == 255) {
		_receiving_sequence_number = realLastSequence;
		return true;
	}
	else {
		return false;
	}
	//return SetSequenceNumber(_receiving_sequence_number, recSeq);
}

bool Receiver::SetSequenceNumber(unsigned char& seqToSet, unsigned char val) {
	unsigned char realLastSequence = val % _seq_num_range;
	if (realLastSequence < _sending_sequence_number) {
		if (realLastSequence) {}
	}
	else {

	}
	//Logger::Log << "Old : " << (int)seqToSet << ", New : " << (int)realLastSequence << "...";
	if ((seqToSet + 1)% _seq_num_range == realLastSequence || seqToSet == 255) {
		seqToSet = realLastSequence;
		//Logger::Log << "TRUE\n";
		return true;
	}
	else {
		//Logger::Log << "FALSE\n";
		return false;
	}
	//unsigned char realLastSequence = 255;
	////if (val > 1) { //Means it's a sequence number up to 255 during handshake
	//	realLastSequence = val % _window_size;
	////}
	///*else {
	//	realLastSequence = val;
	//}*/
	//	Logger::Log << "Old : " << (int)seqToSet << ", New : " << (int)realLastSequence << "...";
	//if (seqToSet != 255 && seqToSet != realLastSequence || seqToSet == 255) {
	//	seqToSet = realLastSequence;
	//	return true;
	//}
	//else {
	//	return false;
	//}
}
void Receiver::InitializeLog(string logName)
{
	//_log = new Logger(logName);
}
bool Receiver::IsReceiving() const
{
	return _receiving;
}

int Receiver::GetPacketsSent() const {
	return _packets_sent;
}
int Receiver::GetPacketsQueued() const {
	return _packets_queued;
}
int Receiver::GetPacketsLost() const {
	return _packets_lost;
}
int Receiver::GetWindowSize() const
{
	return _window_size;
}
void Receiver::ResetCounters() {
	_packets_sent = 0;
	_packets_lost = 0;
	_packets_queued = 0;
}
void Receiver::SetWindowSize(int ws)
{
	_window_size = ws;
	_seq_num_range = ws * 2;
}
void Receiver::SetLossRate(short lr)
{
	_loss_rate = (float)lr * 255 / 100;
}
void Receiver::SetDelayRate(short dr)
{
	_delay_rate = (float)dr * 255 / 100;
}
void Receiver::WaitUntilSendBufferEmpties() const
{
	while (_send_buffer.size() > 0) {
		this_thread::sleep_for(1ms);
		this_thread::yield();
	}
}

void Receiver::ResetSequenceNumbers()
{
	_sending_sequence_number = 255;
	_receiving_sequence_number = 255;
}
